require('./bootstrap');
window.Vue = require('vue');
import VueRouter from 'vue-router'
import VueProgressBar from 'vue-progressbar'
Vue.use(VueRouter);

let routes = [
    { path: '/', component: require('./components/Home.vue').default },
    { path: '/resume', component: require('./components/Resume.vue').default},
    { path: '/services', component: require('./components/Services.vue').default},
];

const options = {
    color: '#f91800',
    failedColor: '#874b4b',
    thickness: '5px',
    transition: {
        speed: '0.2s',
        opacity: '0.6s',
        termination: 300
    },
    autoRevert: true,
    location: 'top',
    inverse: false
}

Vue.use(VueProgressBar, options)
const router = new VueRouter({
    routes,
    mode:"history",
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
})

const app = new Vue({
    el: '#app',
    router,
    mounted() {
        this.$Progress.finish()
    },
    created () {
        this.$Progress.start()
        this.$router.beforeEach((to, from, next) => {
            this.$Progress.start()
            next()
        })

        this.$router.afterEach((to, from) => {
            this.$Progress.finish()
        })
    }
});
