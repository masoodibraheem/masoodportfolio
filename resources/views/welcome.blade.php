<!DOCTYPE HTML>
<html class="no-js" lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="author" content="smartit-source">
    <meta name="description" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <!-- title here -->
    <title>Masood Ibraheem - Best Web developer of Kashmir</title>
    <!-- Favicon and Touch Icons -->
    <link rel="shortcut icon" href="{{asset("assets/images/fav.png")}}">
    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('css/all.css')}}">
</head>
<body>
<div id="app">
<!-- header area start here -->
<header class="header-area" id="sticky">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                <div class="logo-area">
                    <a href="{{url('/')}}"><img src="{{asset("public/assets/images/logo.png")}}" alt="masood ibraheem"></a>
                </div>
            </div>
            <div class="col-md-10">
                <div class="menu-area text-right">
                    <nav class="main-menu">
                        <ul>
                            <li>
                                <router-link to="/">
                                    <span class="flaticon-home-page"></span>Home
                                </router-link>
                            </li>
                            <li><router-link to="/resume">
                                    <span class="flaticon-resume"></span>Resume
                                </router-link></li>
                            <li><router-link to="/services"><span class="flaticon-settings"></span>Services
                                </router-link>
                            </li>
                            <li><a href="portfolio.html"><span class="flaticon-library"></span>Portfolio</a></li>
                            <li><a href="#"><span class="flaticon-website"></span>Blog Page</a></li>
                            <li><a href="contact.html"><span class="flaticon-chat"></span>Contact </a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header area start here -->
<router-view></router-view>
    <vue-progress-bar></vue-progress-bar>
<!-- footer area start here -->
<footer class="footer-area">
    <div class="footer-top section">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2">
                    <div class="footer-top-contet text-center">
                        <div class="footer-logo">
                            <a href="{{url('/')}}">
                                <img src="{{asset("public/assets/images/logo.png")}}"
                                     alt="cvita"></a>
                        </div>
                        <p>We love who we are and we are very proud to be Curabitur sit amet magnaquam. Praesent in libero vel turpis</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="single-address text-center">
                        <div class="address-bar-icon">
                            <span class="flaticon-placeholder"></span>
                        </div>
                        <div class="address-info">
                            <p>7777 modern Avenue, Suite 05, New York,</p>
                            <p>CA 9077, USA 2018 </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-address text-center">
                        <div class="address-bar-icon">
                            <span class="flaticon-telephone"></span>
                        </div>
                        <div class="address-info">
                            <p>Official Fax: 575 051 000</p>
                            <p>Official Phone: 0123 456 789 </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-address text-center">
                        <div class="address-bar-icon">
                            <span class="flaticon-chat-1"></span>
                        </div>
                        <div class="address-info">
                            <p>Email :info@yourwebsite.com</p>
                            <p>website: www.yourwebsite.com</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="copyright-area">
                        <p>&copy; 2018 Copyright createuiux. All rights reserved</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="social-media text-right">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fa fa-behance"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- End js file -->
</div>

<script  src="{{asset('js/app.js')}}"></script>
<script  src="{{asset('js/all.js')}}"></script>
</body>
</html>
