const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .scripts([
        'resources/js/cmjs/modernizr-2.8.3-respond-1.4.2.min.js',
        'resources/js/cmjs/jquery-1.12.0.min.js',
        'resources/js/cmjs/plugins.js',
        'resources/js/cmjs/Popper.js',
        'resources/js/cmjs/boostrap.min.js',
        'resources/js/cmjs/owl.carousel.min.js',
        'resources/js/cmjs/isotope.pkgd.min.js',
        'resources/js/cmjs/imagesloaded.pkgd.min.js',
        'resources/js/cmjs/scrollup.js',
        'resources/js/cmjs/jquery.counterup.min.js',
        'resources/js/cmjs/waypoints.min.js',
        'resources/js/cmjs/jquery.meanmenu.js',
        'resources/js/cmjs/animated-headline.js',
        'resources/js/cmjs/main.js',
    ],'public/js/all.js')
   .sass('resources/sass/app.scss', 'public/css')
   .styles([
       'resources/css/font-awesome.min.css',
       'resources/css/flaticon.css',
       'resources/css/bootstrap.min.css',
       'resources/css/animate.css',
       'resources/css/owl.carousel.css',
       'resources/css/owl.theme.css',
       'resources/css/owl.transitions.css',
       'resources/css/meanmenu.min.css',
       'resources/css/animated-headline.css',
       'resources/css/style.css',
       'resources/css/responsive.css',
   ],'public/css/all.css');

